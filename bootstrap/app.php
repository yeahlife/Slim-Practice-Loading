<?php

require_once  __DIR__ . '/../vendor/autoload.php';

$settings = require_once __DIR__ . '/../src/settings.php';

$app = new \Slim\App($settings);

$container = $app->getContainer();

/**
 *  使用 容器 中的 view 索引 Twig模板对象
 * @param $container
 * @return \Slim\Views\Twig
 */
$container['view'] = function($container){
    $dirname = __DIR__ . '/../resource/views';

    $view = new \Slim\Views\Twig($dirname, [
        'cache' => false
    ]);

    $view->addExtension(new \Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
    ));

    return $view;
};

$container['IndexController'] = function($container){
    return new \App\Controllers\IndexController($container);
};

$container['HomeController'] = function($container){
    return new \App\Controllers\HomeController($container);
};
/**
 * 模型组件注入至 Slim\App 中
 * @param $container
 * @return \Medoo\medoo
 */
$container['db'] = function($container){
    $settings = $container->get('settings');
    return new \Medoo\medoo($settings['mysql']);
};

/**
 *  EasyWeChat组件
 * @param $container
 * @return \EasyWeChat\Foundation\Application
 */
/*$container['WeChat'] = function($container){
    $settings = $container->get('settings');
    return new \EasyWeChat\Foundation\Application($settings['WeChat']);
};*/


$app->add(new \App\Middleware\ValidationMiddleware($container));

require_once __DIR__ . '/../app/routes.php';
