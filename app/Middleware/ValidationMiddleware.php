<?php
/**
 * @package
 * @version
 * @author  <linframe@outlook.com>
 */

namespace App\Middleware;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


class ValidationMiddleware
{

    protected $container;

    public function __construct($container)
    {
        $this->container  = $container;
    }


    public function __invoke(RequestInterface $request,
                             ResponseInterface $response, $nextCallable)
    {
        //if($nextCallable instanceof \Closure) {
        //var_dump('validationmiddileware');
        //$headers = $request->getHeaders();
        //var_dump($headers);
        $response = $nextCallable($request, $response);

        /*return $response->withHeader(
            'X-Clacks-Overhead',
            'wangfulin'
        );*/
        //}
    }
}